/*
 Create an addStudent() function that will accept a name of the student and add it to the student array.

*/
// let studentArray = [];
// function addStudent(studentName){
//   studentArray.push(studentName);
//   console.log(studentName + " was added in the array.");
//   console.log(studentArray); //just to display the array
// }

/*
Create a countStudents() function that will print the total number of students in the array.
*/

// function countStudents(){
//   console.log("There are a total of " + studentArray.length + " students in the array." );
// }

/*
Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
*/

// let studentArray = ['jack', 'jill', 'phoebe', 'edward', 'kyle', 'jake', 'jill', 'bob', 'jack', 'jack'];

// function printStudents(){
//   console.log(studentArray.sort()); //just to display the array.
//   studentArray.forEach(function(student){
//   console.log(student);
// })
// }


/*
Create a findStudent() function that will do the following:
  Search for a student name when a keyword is given (filter method).
  If one match is found print the message studentName is an enrollee.
  If multiple matches are found print the message studentNames are enrollees.
  If no match is found print the message studentName is not an enrollee.
  The keyword given should not be case sensitive.
*/

// let studentArray = ['jack', 'jill', 'phoebe', 'edward', 'kyle', 'jake', 'jill', 'bob', 'jack', 'jack'];
// console.log(studentArray); 
// function findStudent(studentName){
//   let filterStudents = studentArray.filter(function(student){
//     return student.toLowerCase().includes(studentName);
//   })
//   if(filterStudents.length === 1){
//     console.log(filterStudents + " is an enrollee.");
//   }
//   else if(filterStudents.length >= 2){
//     console.log(filterStudents + " are enrollees.");
//   }
//   else{
//     console.log("No match is found. " + studentName + " is not an enrollee.");
//   }
// }

/*
  Optional
Create an addSection() function that will add a section to all students in the array with the format of studentName - Section A (map method).
Create a removeStudent() Function that will do the following:
Capitalize the first letter of the user’s input (toUpperCase and slice methods).
Retrieve the index of the student to be removed (indexOf method).
Remove the student from the array (splice method).
Print a message that the studentName was removed from the student’s list.

*/


// function addSection(section){
//   let studentMap = studentArray.map(function(student){
//   return student + " - Section " + section;
//   });
//   console.log(studentMap);
// }

/*
Create a removeStudent() Function that will do the following:
Capitalize the first letter of the user’s input (toUpperCase and slice methods).
Retrieve the index of the student to be removed (indexOf method).
Remove the student from the array (splice method).
Print a message that the studentName was removed from the student’s list.

*/
// let studentArray = ['jack', 'jill', 'phoebe', 'edward', 'kyle', 'jake', 'jill', 'bob', 'jack', 'jack'];

function removeStudent(studentName){
    console.log(studentArray.indexOf(studentName));
    studentArray.splice(studentArray.indexOf(studentName),1);
    console.log(studentArray);
    console.log(studentName.charAt(0).toUpperCase() + studentName.slice(1) + " was removed from the list.");
}
